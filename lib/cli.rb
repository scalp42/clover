class CloverCli
  include Mixlib::CLI

  def initialize
    super
    check_args
    check_lock
  end

  option :help,
  short: '-h',
  long: '--help',
  description: 'Show this message.',
  on: :tail,
  boolean: true,
  show_options: true,
  exit: 0

  option :log_level,
  short: '-l level',
  long: '--logging LEVEL',
  default: 'INFO',
  description: 'Set the log level (debug, error, info, warn). Defaults to INFO level.',
  proc: proc { |l| l.to_s.upcase }

  option :directory,
  short: '-d directory',
  long: '--directory DIR',
  default: "#{File.expand_path File.dirname(__FILE__)}/../data",
  description: 'The directory to load the flat files from. Defauls to current dir + "/data".'

  option :spec,
  short: '-s spec',
  long: '--spec SPEC',
  description: 'The spec file to use for the data. Must be specified.',
  proc: proc { |f| File.expand_path(f) }

  option :threads,
  long: '--threads THREADS',
  default: nil,
  description: 'Specify the number of threads to run.',
  proc: proc { |t| t.to_i }

  option :database,
  long: '--database DATABASE',
  default: 'clover',
  description: 'Specify the database to connect to.'

  option :table,
  long: '--table TABLE',
  default: nil,
  description: 'Specify the table to create. If not specified, table will default to spec name.'

  option :user,
  long: '--user USER',
  default: 'root',
  description: 'Specify the user to connect to the database.'

  option :password,
  long: '--password PASSWORD',
  default: nil,
  description: 'Specify the password to connect to the database.'

  option :force,
  long: '--force',
  boolean: true,
  default: false,
  description: 'Will drop and recreate table if passed. This is dangerous!'

  option :host,
  long: '--host HOST',
  default: '127.0.0.1',
  description: 'Specify the host to connect to.'

  option :process,
  long: '--process PROCESS',
  default: nil,
  description: 'Specify the number of processes to run.',
  proc: proc { |p| p.to_i }

  def log_levels
    %w(debug error info warn)
  end

  def check_args
    parse_options
    unless log_levels.include?(config[:log_level].downcase)
      logger.error 'Logging level can only be debug, error, info or warn.'
      exit 1
    end
    unless config[:spec] && File.exist?(config[:spec])
      logger.error 'Please specify a valid spec file.'
      exit 1
    end
  rescue OptionParser::MissingArgument
    msg = 'Please verify args, use --help.'
    logger.error msg
    # if using Clover as a library
    # raise CloverErrors::CliError, "#{msg} (#{$!.class})"
    exit 1
  rescue OptionParser::InvalidOption => e
    logger.error %(Invalid argument: #{e.args.join(',')})
    exit 1
  end

  def check_lock
    if config[:lock]
      File.new(config[:lock_file], 'w+').flock(File::LOCK_EX | File::LOCK_NB) || abort('Clover already running.')
    end
  end
end
