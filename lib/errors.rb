class CloverErrors
  class GenericError < RuntimeError
    def initalize(exception)
      super(exception)
    end
  end

  class CliError < GenericError; end
end
