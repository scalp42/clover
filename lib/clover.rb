class Clover
  def initialize(cli, file)
    @cli = cli
    @file = file
    @data = File.read(%|#{cli.config[:directory]}/#{file}.txt|)
    @spec = CSV.read(cli.config[:spec], headers: false)[1..-1]
    create_table && push_to_db
    # ap db[%q|show status where `variable_name` = 'Threads_connected'|].first[:Value]
    db.disconnect
  end

  def db
    @connection ||= Sequel.connect(
      adapter: :mysql2,
      user: @cli.config[:user],
      hosts: @cli.config[:host],
      database: @cli.config[:database],
      max_connections: 1,
      sql_log_level: :debug,
      # logger: Helpers.logger
    )
    begin
      @connection.test_connection
      # ap @connection.object_id
    rescue Sequel::DatabaseConnectionError, Sequel::DatabaseConnectionError => ex
      logger.error %|#{@file} => Could not connect to database, #{ex}|
      return nil
    end
    @connection
  end

  def push_to_db
    spec = {}
    @spec.each do |format|
      column = format.first
      width = format[1]
      type = format.last
      spec[column] ||= {}
      spec[column]['column'] = column
      spec[column]['width'] = width.to_i
      spec[column]['type'] = type
    end

    @data.split("\n").each do |line|
      cursor = 0
      result = {}
      spec.each do |col, specs|
        width = specs['width']
        val = line[cursor..cursor + width - 1]
        cursor += width
        result[col] = val
      end
      db[@file.to_sym].insert(result)
    end
  end

  def create_table
    spec = @spec
    db_exists = db.table_exists?(@file)
    if db_exists && !@cli.config[:force]
      Helpers.logger.warn %|#{@file} => Table already exists, but --force was not passed. Skipping.|
      return nil
    elsif @cli.config[:force]
      db.drop_table?(@file)
    end
    db.create_table? @file do
      spec.each do |format|
        column = format.first
        type = format.last
        send(type.capitalize, column.to_sym)
      end
    end
    true
  end
end
