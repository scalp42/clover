class Main
  def self.init(cli)
    started = Time.now
    Helpers.logger.level = Logger.const_get(cli.config[:log_level])
    files = Dir.glob("#{cli.config[:directory]}/*.txt").map { |p| p.split('/').last.split('.txt').first }.uniq
    if cli.config[:threads]
      Helpers.logger.info %|Starting run with #{cli.config[:threads]} thread#{cli.config[:threads] > 1 ? 's' : ''}.|
      Parallel.map(files, in_threads: cli.config[:thread]) do |file|
        Helpers.benchmark(file, cli.config[:log_level]) { Clover.new(cli, file) }
        nil
      end
    elsif cli.config[:process]
      Helpers.logger.info %|Starting run with #{cli.config[:process]} process#{cli.config[:process] > 1 ? 'es' : ''}.|
      Parallel.map(files, in_threads: cli.config[:thread]) do |file|
        Helpers.benchmark(file, cli.config[:log_level]) { Clover.new(cli, file) }
        nil
      end
    else
      files.each do |file|
        Helpers.logger.info %|Starting sequential run.|
        Helpers.benchmark(file, cli.config[:log_level]) { Clover.new(cli, file) }
      end
    end
    finished = Time.now - started
    Helpers.logger.info %|Run finished in #{finished.round(2)} seconds.|
  end
end
