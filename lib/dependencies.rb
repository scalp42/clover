required_gems = %w(
  logger
  awesome_print
  mixlib/cli
  csv
  mysql2
  sequel
  benchmark
  parallel
)

def gem_error(required_gem)
  logger.error %|The required gem "#{required_gem}" was not found/loaded.|
  exit 1
end

required_gems.each do |required_gem|
  begin
    require required_gem
  rescue LoadError
    gem_error(required_gem)
    exit 1
  end
end
