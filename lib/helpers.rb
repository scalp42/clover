module Helpers
  def self.logger
    $stdout.sync = true
    @logger ||= Logger.new(STDOUT)
    @logger.formatter = proc do |severity, datetime, progname, msg|
      if severity == 'INFO'
        "[#{datetime} | #{severity}]  #{msg}\n"
      else
        "[#{datetime} | #{severity}] #{msg}\n"
      end
    end
    @logger
  end

  def self.benchmark(file, _log_level, &_block)
    result = nil
    seconds = Benchmark.realtime do
      result = yield
    end
    logger.info "#{file} => Done in #{seconds.round(2)} seconds."
    [result, seconds]
  end
end
