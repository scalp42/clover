#!/usr/bin/env ruby

# require_relative 'lib/helpers'
require_relative 'lib/dependencies'

APP_ROOT = File.expand_path('..', __FILE__)

Dir.glob("#{APP_ROOT}/{lib}/*.rb").each { |file| require file }

Main.init(CloverCli.new)
